// Declare the pins for the Button and the LED<br>int buttonPin = 12;
int BUTTON = 4;
int MOSFET = 9;
// Brightness of the light
int aBrightness = 0;

void setup() {
  // for debug
  Serial.begin(9600);
  
  // put your setup code here, to run once:
  // Define BUTTON as input and activate the internal pull-up resistor
   pinMode(BUTTON, INPUT_PULLUP);
   // DefineMOSFET as output, for the LED MOSFTE DRIVER
   pinMode(MOSFET, OUTPUT);

   // LED OFF at startup
   analogWrite(MOSFET,aBrightness);
}

void loop() {
  // put your main code here, to run repeatedly:
  int buttonValue = digitalRead(BUTTON);

  if (buttonValue == LOW){
    aBrightness = aBrightness - 25;
    if (aBrightness<=0){
      aBrightness=255;
    }

  // If button pushed, turn LED on
  Serial.println(aBrightness);
  analogWrite(MOSFET,aBrightness); 
  }

  delay(200);
}
